"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const AgendamentoController_1 = require("./controllers/AgendamentoController");
//Route
class App {
    constructor() {
        this.app = express();
        this.middleware();
        this.routes();
    }
    middleware() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(express.static('public'));
    }
    routes() {
        //Readme on Browser
        this.app.route("/").get((req, res) => {
            res.send("public/index.html");
        });
        //Endpoints
        this.app.route("/api/v1/agendamento").post(AgendamentoController_1.default.create);
        this.app.route("/api/v1/agendamento/:id").delete(AgendamentoController_1.default.delete);
        this.app.route("/api/v1/agendamento").get(AgendamentoController_1.default.get);
        this.app.route("/api/v1/agendamento/disponiveis").get(AgendamentoController_1.default.getAvailable);
    }
}
exports.default = new App();
