"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
class TimeHelper {
    //Valida o Intervalo de Horas
    validInterval(inicio, fim) {
        if (moment(inicio, 'hh:mm').isBefore(moment(fim, 'hh:mm'))) {
            return true;
        }
        return false;
    }
    //Checa se intervalo pode ser inserido
    checkInterval(inicio, fim, rule) {
        let available = true;
        const hrInicio = moment(inicio, 'hh:mm');
        const hrFim = moment(fim, 'hh:mm');
        if (hrInicio.isBetween(moment(rule.inicio, 'hh:mm'), moment(rule.fim, 'hh:mm'))
            || hrFim.isBetween(moment(rule.inicio, 'hh:mm'), moment(rule.fim, 'hh:mm'))
            || hrInicio.isSame(moment(rule.inicio, 'hh:mm'))
            || hrFim.isSame(moment(rule.fim, 'hh:mm'))) {
            available = false;
        }
        return available;
    }
    //Retorna dia da semana como um numero
    getWeekDay(date) {
        return moment(date, "DD MM YYYY").weekday();
    }
}
exports.default = new TimeHelper();
