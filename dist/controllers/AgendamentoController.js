"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dbHelper_1 = require("../helpers/dbHelper");
const moment = require("moment");
const timeHelper_1 = require("../helpers/timeHelper");
const DailyController_1 = require("./DailyController");
const DateController_1 = require("./DateController");
const WeeklyController_1 = require("./WeeklyController");
//Classe Controller das requisições
class AgendamentoController {
    //Cria nova regra
    create(req, res) {
        let content = dbHelper_1.default.getContent();
        let validRule = true;
        //Checa Integridade da requisição
        if (req.body.semanalmente && req.body.diariamente
            || req.body.diariamente && req.body.data
            || req.body.data && req.body.semanalmente) {
            return res.status(400).send({ msg: 'Erro , mais de um tipo de regra enviado .' });
        }
        else 
        //Checa de o Banco está vazio
        if (content.agendamentos.length) {
            //Checa de O Intervalo de Horas é Válido
            if (timeHelper_1.default.validInterval(req.body.inicio, req.body.fim)) {
                //Escolhe regra baseado na requisição
                if (req.body.semanalmente) {
                    validRule = WeeklyController_1.default.checkWeekly(req.body, content.agendamentos);
                }
                else if (req.body.diariamente) {
                    validRule = DailyController_1.default.checkDaily(req.body.inicio, req.body.fim, content.agendamentos);
                }
                else if (req.body.data) {
                    validRule = DateController_1.default.checkDate(req.body, content.agendamentos);
                }
            }
            else {
                return res.status(400).send({ msg: 'Intervalo de Horas Inválido.' });
            }
        }
        if (validRule) {
            dbHelper_1.default.save(req.body);
            return res.status(200).send({ msg: 'Nova Regra Cadastrada.' });
        }
        else {
            return res.status(400).send({ msg: 'Erro ao cadastrar regra , dados inválidos.' });
        }
    }
    //Deleta Regra Baseada no ID
    delete(req, res) {
        let deleted = dbHelper_1.default.delete(req.params.id);
        if (!deleted) {
            return res.status(400).send({ msg: 'Nenhuma Regra Deletada.' });
        }
        return res.status(200).send({ msg: 'Regra Deletada.' });
    }
    //Lista Todas as Regras
    get(req, res) {
        return res.send(dbHelper_1.default.getContent());
    }
    //Lista Intervalo de Haras Disponíveis Separados por data
    getAvailable(req, res) {
        class AvailableInterval {
        }
        ;
        class HourInterval {
            constructor(start, end) { this.start = start; this.end = end; }
        }
        ;
        let regras = dbHelper_1.default.getContent().agendamentos;
        let dataInicio = moment(req.body.dataInicio, "DD MM YYYY");
        let dataFim = moment(req.body.dataFim, "DD MM YYYY");
        if (dataInicio.isAfter(dataFim)) {
            return res.status(400).send({ msg: "Data Final Anterior à Inicial." });
        }
        let dataAtual = dataInicio;
        //Intervalo de dias para iteração
        var duration = moment.duration(dataFim.diff(dataInicio)).asDays();
        let result = [];
        for (var i = 0; i <= duration; i++) {
            var interval = new AvailableInterval();
            interval.day = dataAtual.format("DD-MM-YYYY");
            interval.intervals = [];
            let dayWeek = dataAtual.weekday();
            for (var j = 0; j < regras.length; j++) {
                if (regras[j].semanalmente) {
                    //Seleciona Dia da Semana e Checa Regra
                    switch (dayWeek) {
                        case 0: {
                            if (regras[j].domingo) {
                                interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                            }
                            break;
                        }
                        case 1: {
                            if (regras[j].segunda) {
                                interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                            }
                            break;
                        }
                        case 2: {
                            if (regras[j].terca) {
                                interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                            }
                            break;
                        }
                        case 3: {
                            if (regras[j].quarta) {
                                interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                            }
                            break;
                        }
                        case 4: {
                            if (regras[j].quinta) {
                                interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                            }
                            break;
                        }
                        case 5: {
                            if (regras[j].sexta) {
                                interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                            }
                            break;
                        }
                        case 6: {
                            if (regras[j].sabado) {
                                interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                            }
                            break;
                        }
                    }
                }
                else if (regras[j].diariamente) {
                    interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                }
                else {
                    if (regras[j].data && dataAtual.isSame(moment(regras[j].data, "DD MM YYYY"))) {
                        interval.intervals.push(new HourInterval(regras[j].inicio, regras[j].fim));
                    }
                }
            }
            result.push(interval);
            dataAtual = dataAtual.add(1, 'days');
        }
        return res.status(200).send(result);
    }
}
exports.default = new AgendamentoController();
