"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const timeHelper_1 = require("../helpers/timeHelper");
//Classe Para Regras por data
class DateController {
    checkDate(rule, content) {
        let available = true;
        let dayWeek = timeHelper_1.default.getWeekDay(rule.data);
        for (var i = 0; i < content.length; i++) {
            if (content[i].data && content[i].data == rule.data || content[i].diariamente) {
                //Checa de O intervalo pode ser inserido
                available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
            }
            else if (content[i].semanalmente) {
                switch (dayWeek) {
                    case 0: {
                        if (content[i].domingo) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 1: {
                        if (content[i].segunda) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 2: {
                        if (content[i].terca) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 3: {
                        if (content[i].quarta) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 4: {
                        if (content[i].quinta) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 5: {
                        if (content[i].sexta) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 6: {
                        if (content[i].sabado) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                }
            }
            if (!available) {
                return false;
            }
        }
        return available;
    }
}
exports.default = new DateController();
