"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const timeHelper_1 = require("../helpers/timeHelper");
//Classe Para Regras semanalmente
class WeeklyController {
    checkWeekly(rule, content) {
        let available = true;
        for (var i = 0; i < content.length; i++) {
            if (content[i].diariamente) {
                available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
            }
            else if (content[i].semanalmente) {
                //Checa se os dias da semana conferem e podem serem inseridos
                if (content[i].segunda && rule.segunda
                    || content[i].terca && rule.terca
                    || content[i].quarta && rule.quarta
                    || content[i].quinta && rule.quinta
                    || content[i].sexta && rule.sexta
                    || content[i].sabado && rule.sabado
                    || content[i].domingo && rule.domingo) {
                    available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                }
            }
            else if (content[i].data) {
                let dayWeek = timeHelper_1.default.getWeekDay(content[i].data);
                switch (dayWeek) {
                    case 0: {
                        if (rule.domingo) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 1: {
                        if (rule.segunda) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 2: {
                        if (rule.terca) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 3: {
                        if (rule.quarta) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 4: {
                        if (rule.quinta) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 5: {
                        if (rule.sexta) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 6: {
                        if (rule.sabado) {
                            available = timeHelper_1.default.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                }
            }
            if (!available) {
                return false;
            }
        }
        return available;
    }
}
exports.default = new WeeklyController();
