"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const timeHelper_1 = require("../helpers/timeHelper");
//Classe Para Regras diariamente
class DailyController {
    checkDaily(inicio, fim, content) {
        let available = true;
        for (var i = 0; i < content.length; i++) {
            //Checa de O intervalo pode ser inserido
            available = timeHelper_1.default.checkInterval(inicio, fim, content[i]);
            if (!available) {
                return false;
            }
        }
        return available;
    }
}
exports.default = new DailyController();
