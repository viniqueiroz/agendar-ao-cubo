import timeHelper from '../helpers/timeHelper';
//Classe Para Regras semanalmente
class WeeklyController {

    public checkWeekly(rule: any, content: []): boolean {
        let available = true;
        for (var i = 0; i < content.length; i++) {
            if (content[i].diariamente) {
                available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
            } else if (content[i].semanalmente) {
                //Checa se os dias da semana conferem e podem serem inseridos
                if (content[i].segunda && rule.segunda
                    || content[i].terca && rule.terca
                    || content[i].quarta && rule.quarta
                    || content[i].quinta && rule.quinta
                    || content[i].sexta && rule.sexta
                    || content[i].sabado && rule.sabado
                    || content[i].domingo && rule.domingo) {
                    available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                }
            } else if (content[i].data) {
                let dayWeek = timeHelper.getWeekDay(content[i].data);

                switch (dayWeek) {
                    case 0: {
                        if (rule.domingo) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 1: {
                        if (rule.segunda) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 2: {
                        if (rule.terca) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 3: {
                        if (rule.quarta) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 4: {
                        if (rule.quinta) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 5: {
                        if (rule.sexta) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 6: {
                        if (rule.sabado) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                }

            }
            if (!available) {

                return false;
            }
        }
        return available;
    }


}
export default new WeeklyController();