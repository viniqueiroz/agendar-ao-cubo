import timeHelper from '../helpers/timeHelper';
//Classe Para Regras por data
class DateController {

    public checkDate(rule: any, content: []): boolean {
        let available = true;
        let dayWeek = timeHelper.getWeekDay(rule.data);
        for (var i = 0; i < content.length; i++) {
            if (content[i].data && content[i].data == rule.data || content[i].diariamente) {
                //Checa de O intervalo pode ser inserido
                available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
            } else if (content[i].semanalmente) {
                switch (dayWeek) {
                    case 0: {
                        if (content[i].domingo) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 1: {
                        if (content[i].segunda) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 2: {
                        if (content[i].terca) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 3: {
                        if (content[i].quarta) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 4: {
                        if (content[i].quinta) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 5: {
                        if (content[i].sexta) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                    case 6: {
                        if (content[i].sabado) {
                            available = timeHelper.checkInterval(rule.inicio, rule.fim, content[i]);
                        }
                        break;
                    }
                }

            }
            if (!available) {
                return false;
            }
        }
        return available;
    }


}
export default new DateController();