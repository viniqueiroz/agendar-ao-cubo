import timeHelper from '../helpers/timeHelper';
//Classe Para Regras diariamente
class DailyController {

    public checkDaily(inicio: string, fim: string, content: []): boolean {
        let available = true;
        for (var i = 0; i < content.length; i++) {
            //Checa de O intervalo pode ser inserido
            available = timeHelper.checkInterval(inicio, fim, content[i]);
            if (!available) {
                return false;
            }
        }
        return available;
    }


}
export default new DailyController();