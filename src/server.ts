import App from "./app";

let port = process.env.PORT || '8000';

App.app.listen(port, function () {
  console.log(`Servidor rodando em: http://localhost:${port}`);
});

