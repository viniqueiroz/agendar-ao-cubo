import moment = require('moment');
class TimeHelper {
    //Valida o Intervalo de Horas
    public validInterval(inicio: string, fim: string): boolean {
        if (moment(inicio, 'hh:mm').isBefore(moment(fim, 'hh:mm'))) {
            return true;
        }
        return false;
    }
    //Checa se intervalo pode ser inserido
    public checkInterval(inicio: string, fim: string, rule: any): boolean {
        let available = true;
        const hrInicio = moment(inicio, 'hh:mm');
        const hrFim = moment(fim, 'hh:mm');

        if (hrInicio.isBetween(moment(rule.inicio, 'hh:mm'), moment(rule.fim, 'hh:mm'))
            || hrFim.isBetween(moment(rule.inicio, 'hh:mm'), moment(rule.fim, 'hh:mm'))
            || hrInicio.isSame(moment(rule.inicio, 'hh:mm'))
            || hrFim.isSame(moment(rule.fim, 'hh:mm'))
        ) {
            available = false;
        }
        return available;
    }
    //Retorna dia da semana como um numero
    public getWeekDay(date: string) {
        return moment(date, "DD MM YYYY").weekday();
    }

}
export default new TimeHelper();