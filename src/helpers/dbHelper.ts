const fs = require('fs');
const filePath = __dirname + '/../../src/schemas/agendamentos.json';
//Classe de manipulação do banco
class DbHelper {
    //Salva no banco
    public save(regra: any) {
        //ID baseano no timestamp
        regra.id = new Date().getTime();

        let regras = this.getContent();
        regras.agendamentos.push(regra);
        let data = JSON.stringify(regras);
        this.saveContent(data);
    }
    //Deleta do banco
    public delete(id: any) {
        let regras = this.getContent();
        let toSave = { agendamentos: [] };
        let deleted = true;
        regras.agendamentos.forEach(agendamento => {
            if (agendamento.id != id) {
                toSave.agendamentos.push(agendamento);
            }
        });

        let data = JSON.stringify(toSave);
        this.saveContent(data);
        if (regras.agendamentos.length == toSave.agendamentos.length) {
            deleted = false;
        }
        return deleted;
    }
    //Retorna todas as regras do banco
    public getContent(): any {
        let content = fs.readFileSync(filePath, 'utf8');

        return JSON.parse(content);
    }
    //Salva propriamente no Banco
    private saveContent(data: any): any {
        let response = fs.writeFileSync(filePath, data, 'utf8');

        return response;
    }
}
export default new DbHelper();