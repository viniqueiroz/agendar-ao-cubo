import * as express from "express";
import * as bodyParser from "body-parser";
import AgendamentoController from './controllers/AgendamentoController';



//Route

class App {
  public app: any;
  constructor() {
    this.app = express();
    this.middleware();
    this.routes();
  }

  middleware() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(express.static('public'));
  }

  routes() {
    //Readme on Browser
    this.app.route("/").get((req, res) => {
      res.send("public/index.html")
    });


    //Endpoints
    this.app.route("/api/v1/agendamento").post(AgendamentoController.create);
    this.app.route("/api/v1/agendamento/:id").delete(AgendamentoController.delete);
    this.app.route("/api/v1/agendamento").get(AgendamentoController.get);
    this.app.route("/api/v1/agendamento/disponiveis").get(AgendamentoController.getAvailable);


  }
}

export default new App();
